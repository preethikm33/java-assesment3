package one;
class teacher
{
   private String teacherName;
   private String subject;
   private double salary;

    public teacher(String teacherName, String subject, double salary) {
		super();
		this.teacherName = teacherName;
		this.subject = subject;
		this.salary = salary;
	}
   public String getTeacherName() {
		return teacherName;
	}
 public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
  public String getSubject() {
		return subject;
	}
 public void setSubject(String subject) {
		this.subject = subject;
	}
 public double getSalary() {
		return salary;
	}
public void setSalary(double salary) {
		this.salary = salary;
	}
	}

public class Arraysassesment {
public static void main(String[] args) {
		
		teacher t1=new teacher("Alex","java fundamentals",1200L);
		teacher t2=new teacher("John","RDBMS",800L);
		teacher t3=new teacher("Sam","java networking",900L);
		teacher t4=new teacher("Maria","python",900L);
		teacher[] T= {t1,t2,t3,t4};
		for(int i=0;i<T.length;i++)
		{
			System.out.println("Name : "+T[i].getTeacherName()+", Subject : "+T[i].getSubject()+", Salary : "+T[i].getSalary());
		}
		
	}

	
}
